# IIT KGP FOUNDATION OF AIML COURSE VIDEOS

## VIDEO SET 1

| SL# | TOPICS |
|:---:| :----- |
|  1  | Data Structure And Alogorithm Part 1 |
|  2  | Data Structure And Alogorithm Part 2 |
|  3  | Data Structure And Alogorithm Part 3 |
|  4  | Introduction to Probability |
|  5  | Linear Algebra Part 1 |
|  6  | Linear Algebra Part 2 |
